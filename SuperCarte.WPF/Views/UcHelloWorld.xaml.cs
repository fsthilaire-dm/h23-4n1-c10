﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SuperCarte.WPF.Views
{
    /// <summary>
    /// Logique d'interaction pour UcHelloWorld.xaml
    /// </summary>
    public partial class UcHelloWorld : UserControl
    {
        public UcHelloWorld()
        {
            InitializeComponent();

            var a = CultureInfo.CurrentUICulture;
            var b = CultureInfo.CurrentCulture;
            var c = CultureInfo.DefaultThreadCurrentCulture;

            //CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("fr-CA");           

        }

        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if(DataContext != null)
            {
                if(DataContext is HelloWordVM)
                {
                    await ((HelloWordVM)DataContext).RafraichirDateHeureCommande.ExecuteAsync(null);
                }
            }
        }
    }
}
