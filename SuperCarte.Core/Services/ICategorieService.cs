﻿using SuperCarte.Core.Models;

namespace SuperCarte.Core.Services;

/// <summary>
/// Interface qui contient les services du modèle Categorie
/// </summary>
public interface ICategorieService
{
    /// <summary>
    /// Obtenir la liste de catégories en asynchrone.
    /// </summary>
    /// <returns>Liste de catégories</returns>
    Task<List<CategorieModel>> ObtenirListeAsync();    

    /// <summary>
    /// Obtenir les dépendances d'une catégorie.
    /// </summary>
    /// <param name="categorieId">Clé primaire de la catégorie</param>
    /// <returns>Les dépendances ou null si la catégorie n'est pas trouvée</returns>
    CategorieDependance? ObtenirDependance(int categorieId);

    /// <summary>
    /// Supprimer une catégorie en asynchrone.
    /// </summary>    
    /// <param name="categorieId">Clé primaire de la catégorie</param>    
    Task SupprimerAsync(int categorieId);
}