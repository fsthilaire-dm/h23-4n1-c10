﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SuperCarte.EF.Migrations
{
    /// <inheritdoc />
    public partial class AjouterTableUtilisateurCarte : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UtilisateurCarte",
                columns: table => new
                {
                    UtilisateurId = table.Column<int>(type: "int", nullable: false),
                    CarteId = table.Column<int>(type: "int", nullable: false),
                    Quantite = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UtilisateurCarte", x => new { x.UtilisateurId, x.CarteId });
                    table.ForeignKey(
                        name: "FK_UtilisateurCarte_Carte_CarteId",
                        column: x => x.CarteId,
                        principalTable: "Carte",
                        principalColumn: "CarteId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UtilisateurCarte_Utilisateur_UtilisateurId",
                        column: x => x.UtilisateurId,
                        principalTable: "Utilisateur",
                        principalColumn: "UtilisateurId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UtilisateurCarte_CarteId",
                table: "UtilisateurCarte",
                column: "CarteId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UtilisateurCarte");
        }
    }
}
