﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SuperCarte.EF.Migrations
{
    /// <inheritdoc />
    public partial class RenommerTables : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CarteTb_CategorieTb_CategorieId",
                table: "CarteTb");

            migrationBuilder.DropForeignKey(
                name: "FK_CarteTb_EnsembleTb_EnsembleId",
                table: "CarteTb");

            migrationBuilder.DropForeignKey(
                name: "FK_UtilisateurTb_RoleTb_RoleId",
                table: "UtilisateurTb");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UtilisateurTb",
                table: "UtilisateurTb");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RoleTb",
                table: "RoleTb");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EnsembleTb",
                table: "EnsembleTb");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CategorieTb",
                table: "CategorieTb");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CarteTb",
                table: "CarteTb");

            migrationBuilder.RenameTable(
                name: "UtilisateurTb",
                newName: "Utilisateur");

            migrationBuilder.RenameTable(
                name: "RoleTb",
                newName: "Role");

            migrationBuilder.RenameTable(
                name: "EnsembleTb",
                newName: "Ensemble");

            migrationBuilder.RenameTable(
                name: "CategorieTb",
                newName: "Categorie");

            migrationBuilder.RenameTable(
                name: "CarteTb",
                newName: "Carte");

            migrationBuilder.RenameIndex(
                name: "IX_UtilisateurTb_RoleId",
                table: "Utilisateur",
                newName: "IX_Utilisateur_RoleId");

            migrationBuilder.RenameIndex(
                name: "IX_CarteTb_EnsembleId",
                table: "Carte",
                newName: "IX_Carte_EnsembleId");

            migrationBuilder.RenameIndex(
                name: "IX_CarteTb_CategorieId",
                table: "Carte",
                newName: "IX_Carte_CategorieId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Utilisateur",
                table: "Utilisateur",
                column: "UtilisateurId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Role",
                table: "Role",
                column: "RoleId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Ensemble",
                table: "Ensemble",
                column: "EnsembleId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Categorie",
                table: "Categorie",
                column: "CategorieId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Carte",
                table: "Carte",
                column: "CarteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Carte_Categorie_CategorieId",
                table: "Carte",
                column: "CategorieId",
                principalTable: "Categorie",
                principalColumn: "CategorieId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Carte_Ensemble_EnsembleId",
                table: "Carte",
                column: "EnsembleId",
                principalTable: "Ensemble",
                principalColumn: "EnsembleId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Utilisateur_Role_RoleId",
                table: "Utilisateur",
                column: "RoleId",
                principalTable: "Role",
                principalColumn: "RoleId",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Carte_Categorie_CategorieId",
                table: "Carte");

            migrationBuilder.DropForeignKey(
                name: "FK_Carte_Ensemble_EnsembleId",
                table: "Carte");

            migrationBuilder.DropForeignKey(
                name: "FK_Utilisateur_Role_RoleId",
                table: "Utilisateur");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Utilisateur",
                table: "Utilisateur");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Role",
                table: "Role");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Ensemble",
                table: "Ensemble");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Categorie",
                table: "Categorie");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Carte",
                table: "Carte");

            migrationBuilder.RenameTable(
                name: "Utilisateur",
                newName: "UtilisateurTb");

            migrationBuilder.RenameTable(
                name: "Role",
                newName: "RoleTb");

            migrationBuilder.RenameTable(
                name: "Ensemble",
                newName: "EnsembleTb");

            migrationBuilder.RenameTable(
                name: "Categorie",
                newName: "CategorieTb");

            migrationBuilder.RenameTable(
                name: "Carte",
                newName: "CarteTb");

            migrationBuilder.RenameIndex(
                name: "IX_Utilisateur_RoleId",
                table: "UtilisateurTb",
                newName: "IX_UtilisateurTb_RoleId");

            migrationBuilder.RenameIndex(
                name: "IX_Carte_EnsembleId",
                table: "CarteTb",
                newName: "IX_CarteTb_EnsembleId");

            migrationBuilder.RenameIndex(
                name: "IX_Carte_CategorieId",
                table: "CarteTb",
                newName: "IX_CarteTb_CategorieId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UtilisateurTb",
                table: "UtilisateurTb",
                column: "UtilisateurId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RoleTb",
                table: "RoleTb",
                column: "RoleId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EnsembleTb",
                table: "EnsembleTb",
                column: "EnsembleId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CategorieTb",
                table: "CategorieTb",
                column: "CategorieId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CarteTb",
                table: "CarteTb",
                column: "CarteId");

            migrationBuilder.AddForeignKey(
                name: "FK_CarteTb_CategorieTb_CategorieId",
                table: "CarteTb",
                column: "CategorieId",
                principalTable: "CategorieTb",
                principalColumn: "CategorieId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CarteTb_EnsembleTb_EnsembleId",
                table: "CarteTb",
                column: "EnsembleId",
                principalTable: "EnsembleTb",
                principalColumn: "EnsembleId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UtilisateurTb_RoleTb_RoleId",
                table: "UtilisateurTb",
                column: "RoleId",
                principalTable: "RoleTb",
                principalColumn: "RoleId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
